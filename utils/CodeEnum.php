<?php 

class CodeEnum
{
    private $_lang='';
    //constant variables
    const   //global naming of messages used to save in session
            FORM            = 'form', 
            SESSION         = 'session',
            DEBUG           = 'debug',
            ERROR           = 'error',
            WARN            = 'warning',
            //Validate error messages
            VAL_MINLEN      = 'val_minlen',
            VAL_MAXLEN      = 'val_maxlen',
            VAL_DIGIT       = 'val_digit',
            VAL_EMAIL       = 'val_email',
            VAL_VALUES      = 'val_values',
            VAL_EQUALS      = 'val_equals',
            VAL_EMPTY       = 'val_empty',
            VAL_LOGIN_OK    = 'val_user_ok',
            VAL_LOGIN_FAILED= 'val_login_failed',
            VAL_CALL_ERROR  = 'val_call_error',
            VAL_OK          = 'val_ok',
            //Session error messages
            SES_UID         = 'ses_uid',
            SES_IP          = 'ses_ip',
            SES_UA          = 'ses_ua',
            SES_EXP         = 'ses_exp',
            SES_NOT_LOGGED  = 'ses_not_logged',
            //USER DEFINED
            PROJECT_OK      = 'project_ok',
            PROJECT_FAILED  = 'project_failed';

    private $_messages = array(
        'cs' => array(
            self::VAL_MINLEN        => ' je příliš krátká. Minimální délka je: ',
            self::VAL_MAXLEN        => ' je příliš dlouhá. Maximální délka je: ',
            self::VAL_DIGIT         => ' musí být číslo.',
            self::VAL_EMAIL         => ' není validní e-mailová adresa.',
            self::VAL_VALUES        => ' obsahuje neplatné hodnoty.',
            self::VAL_EQUALS        => ' se neshoduje.',
            self::VAL_EMPTY         => ' nesmí být prázdné.',
            self::VAL_LOGIN_FAILED  => 'Neplatné uživatelské jméno nebo heslo.',
            self::VAL_CALL_ERROR    => ' Metoda neexistuje ve třídě: ',

            self::SES_UID           => 'Pro uživatele nebyla započata relace.',
            self::SES_IP            => 'Nalezeny rozdílné IP adresy (možný pokus o útok na stránky).',
            self::SES_UA            => 'Nalezeny rozdílné prohlížeče (možný pokus o útok na stránky).',
            self::SES_EXP           => 'Pokus o použití neplatné relace.',
            self::SES_NOT_LOGGED    => 'Pokus o přihlášení neexistujícího uživatele.',

            self::PROJECT_OK        => 'Projekt byl úspěšně vytvořen.',
            self::PROJECT_FAILED    => 'Projekt s tímto názvem již existuje.'
        ),
        'en' => array(
            self::VAL_MINLEN        => ' is too short. Minimal length is: ',
            self::VAL_MAXLEN        => ' is too long. Maximal length is: ',
            self::VAL_DIGIT         => ' must be a digit.',
            self::VAL_EMAIL         => ' is not valid e-mail address.',
            self::VAL_VALUES        => ' contains invalid values.',
            self::VAL_EQUALS        => ' is not equal.',
            self::VAL_EMPTY         => ' can not be emtpy.',
            self::VAL_LOGIN_FAILED  => 'Wrong username or password.',
            self::VAL_CALL_ERROR    => 'Method does not exist inside of: ',

            self::SES_UID           => 'No session started.',
            self::SES_IP            => 'IP Address mixmatch (possible session hijacking attempt).',
            self::SES_UA            => 'Useragent mixmatch (possible session hijacking attempt).',
            self::SES_EXP           => 'Attempt to use expired session.',
            self::SES_NOT_LOGGED    => 'Attempted to log in user that does not exist.',

            self::PROJECT_OK        => 'Project created successfully.',
            self::PROJECT_FAILED    => 'Project with given name already exist.'
        ),
        'de' => array(

        )
    );

    public function __construct($lang='cs') {
        switch ($lang) {
            case 'cs':
                $this->_lang = $lang;
                break;
            case 'en':
                $this->_lang = $lang;
                break;
            case 'de':
                $this->_lang = $lang;
                break;
            default:
                $this->_lang = 'cs';
                break;
        }
    }

    public function getMessage($identifier) {
        return $this->_messages[$this->_lang][$identifier];
    }
 }

 ?>