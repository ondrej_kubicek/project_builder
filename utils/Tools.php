<?php 

/**
* Namespace provides three different classes that implement static methods.
* Name of these classes is chosen to simulate the place in code where they should be used.
* 
* Example of use:
*     Tools\Code::redirect('index'); - if used this way, calls static method from Code class in namespace Tools that ensures redirecting to given url
*     Tools\Debug::formatedPrint(array('foo', 'bar')); - Debug methods should be used in debugging phase only, provides methods for clearly echoing out results and other things
*/
namespace Tools
{
    /**
     * Class providing static methods for debugging purposes.
     */
    class Debug {
        /**
         * Pretty print given array.
         * 
         * @param  array $data Array to print
         */
        public static function formatedPrint($data)
        {
            echo '<pre style="font-family: \'Courier New\'; font-size: 15px; font-weight: normal;">';
            print_r($data);
            echo '</pre>';
        }
    }

    /**
     * Class providing static methods that could be used in code eg. for redirecting user.
     */
    class Code {
        /**
         * Redirects user from current page to defined location. Additional informations like replace and responseCode can be set.
         * 
         * Example of use:
         *     Tools\Code::redirect('index'); - redirects user to http://example.com/index
         * 
         * @param  String  $location        Target page where you want to be redirected
         * @param  String  $targetWeb       (Optional) If set, redirects user to defined web address
         * @param  boolean $replace         The optional replace parameter indicates whether the header should replace a previous similar header, or add a second header of the same type. By default it will replace, but if you pass in FALSE as the second argument you can force multiple headers of the same type
         * @param  integer $responseCode    (Optional) Forces the HTTP response code to the specified value. Note that this parameter only has an effect if the string is not empty
         */
        public static function redirect($location, $targetWeb=URL, $replace=true, $responseCode=302)
        {
            header('Location:' . $targetWeb . $location, $replace, $responseCode);
            exit;
        }

        /**
         * Randomly calls given function with certain probability.
         * 
         * @param  float $probability       Probability for executing the function. Accepts interval 0.1 - 100, where number represents %
         * @param  mixed $class             Object or string defining class the method belongs to
         * @param  string $functionName     Name of function you want to randomly call
         * @param  array $functionArgs      Array of function arguments
         */
        public static function randomFunctionExecute($probability, $class, $functionName, $functionArgs)
        {
            if (method_exists($className, $functionName)) {
                if (rand(1, 1000) <= (10 * $probability))
                {
                    call_user_func_array(array($class, $functionName), $functionArgs);
                }
            }
        }
    }

    /**
     * Class providing static methods that should be used in View context, eg. build <a> tag with needed attributes.
     */
    class View {
        /**
         * Creates anchor tag in view context.
         * 
         * @param  String $link     URL for link to point at
         * @param  String $text     Text of link
         * @param  String $title    Text for title parameter
         * @param  mixed  $extras   (Optional) Array or String used to declare id, class or/and target for link
         * @param  String $domain   Target domain, by default is set to current site adress
         * @return String           Builded "a" tag
         */
        public static function anchor($link, $text, $title, $extras=null, $domain=URL)
        {
            $link = $domain . $link;
            $data = '<a href="' . $link . '"';
             
            if ($title)
            {
                $data .= ' title="' . $title . '"';
            }
            else
            {
                $data .= ' title="' . $text . '"';
            }
            
            if (is_array($extras))
            {
                foreach($extras as $rule)
                {
                    $data .= View::parseExtras($rule);
                }
            }
             
            if (is_string($extras))
            {
                $data .= View::parseExtras($extras);
            }
                 
            $data.= '>';
             
            $data .= $text;
            $data .= "</a>";
             
            return $data;
        }

        /**
         * Creates mailto link in view context.
         * 
         * @param  String $email    Target email adress
         * @param  String $text     Text of link
         * @param  String $title    Text for title parameter
         * @param  mixed  $extras   (Optional) Array or String used to declare id, class or/and target for link
         * @return String           Builded "a" tag with mailing function
         */
        public static function mailTo($email, $text, $title, $extras=null)
        {
            $link = '<a href=\"mailto:' . $email;
            $link = str_rot13($link);
             
            $data = '<script type="text/javascript">document.write("';
            $data .= $link;
            $data .= '".replace(/[a-zA-Z]/g, function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}));';
            $data .= '</script>"';
             
            if ($title)
            {
                $data .= ' title="' . $title . '"';
            }
            else
            {
                $data .= ' title="' . $text . '"';
            }
             
            if (is_array($extras))
            {
                foreach($extras as $rule)
                {
                    $data .= View::parseExtras($rule);
                }
            }
             
            if (is_string($extras))
            {
                $data .= View::parseExtras($extras);
            }
            $data .= ">";
             
            $data .= $text;
            $data .= "</a>";
            return $data;
        }

        /**
         * Help function for anchor function. Parses data passed by extras parameter. Is able to add id, class and target tags to link tag.
         * 
         * @param  String $rule Name of extra parameter for link. Should start with - # . _ characters
         * @return String       Result of parsed data.
         */
        private function parseExtras($rule)
        {
            if ($rule[0] == "#")
            {
                $id = substr($rule,1,strlen($rule));
                $data = ' id="' . $id . '"';
                return $data;
            }
             
            if ($rule[0] == ".")
            {
                $class = substr($rule,1,strlen($rule));
                $data = ' class="' . $class . '"';
                return $data;
            }
             
            if ($rule[0] == "_")
            {
                $data = ' target="' . $rule . '"';
                return $data;
            }
        }
    }
}

 ?>