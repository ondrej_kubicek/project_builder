<?php 

class Auth
{
    public static function handleLogin($targetPage='login', $roleCheck = null)
    {
        $logged = Session::get('logged_in');
        $userId = Session::get('user_id');

        if ($logged == false || $userId == false || ($roleCheck && $roleCheck != $_SESSION['role'])) {
            Session::destroy();
            Tools\Code::redirect($targetPage);
        }
    }

    public static function redirectLoggedUser($targetPage='login')
    {
        $logged = Session::get('logged_in');
        $userId = Session::get('user_id');

        if ($logged && $userId) {
            Tools\Code::redirect($targetPage);
        }
    }
}

 ?>