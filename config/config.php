<?php 

// DATABASE SETTINGS
define ('DB_TYPE', 'mysql');
define ('DB_HOST', 'localhost');
define ('DB_NAME', 'project_builder');
define ('DB_USER', 'root');
define ('DB_PASS', '');

define('DB_SETTINGS', DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8');

// PATHS
define('URL', 'http://localhost/pb/project_builder/');
define('LIBS', 'libs/');

//SESSION VARIABLES
define('S_EXPIRE', 0);

//SERVER SPECIFIC VARIABLES
define ('PROJECT_NAME', 'Instant');
define ('ROOT_FOLDER', 'project_builder');
 ?>