<?php 
//Classes outside libs folder should be manualy loaded here.
require 'config/config.php';
require 'utils/Auth.php';
require 'utils/Tools.php';
require 'utils/CodeEnum.php';
require 'libs/Form/Validate.php';

//AUTOLOADER for classes in libs folder
function __autoload($class) {
    if (!file_exists(LIBS . $class . '.php'))
        return false;
    require LIBS . $class . '.php';
}

$bootstrap = new Bootstrap();

// Optional paths
$bootstrap->setControllerPath('controllers');
$bootstrap->setModelPath('models');
$bootstrap->setErrorFile('error.php');
$bootstrap->setDefaultFile('index.php');

$bootstrap->init();

 ?>