<?php 

class All extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->view->title = "All projects";
		$this->view->message = "Projects overview";
		$this->view->projects = $this->getAllProjects();

		$this->view->render('all/index', 'header', null);
	}

	public function getAllProjects()
	{
		return $this->model->getAllProjects();
	}
}

 ?>