<?php 

class Index  extends Controller 
{
	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$this->view->title = "Projects overview";
		$this->view->message = "Overview";
		$this->view->pageHeader = "Recently viewed projects";

		$this->view->render('index/index', 'header', null);
	}
}

 ?>
