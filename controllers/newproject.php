<?php 

class NewProject  extends Controller 
{
	function __construct() {
		parent::__construct();

		$this->view->js = array('newproject/js/custom.js');
	}

	public function index()
	{
		$this->view->title = "Create new project Structure";
		$this->view->message = "Create new project";

		$this->view->render('newproject/index');
	}

	public function create()
	{
		Session::init();
		Session::setSystemMessage(CodeEnum::FORM, $this->model->create());
	
		Tools\Code::redirect('newproject');
	}
}

 ?>
