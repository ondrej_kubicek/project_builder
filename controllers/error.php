<?php 

class Error extends Controller
{
    
    function __construct()
    {
        parent::__construct();
    }

    public function index($title=false, $message=false)
    {
        if (!$title) $this->view->title = "404 Error";
        else $this->view->title = $title;

        if (!$message) $this->view->message = "This page does not exist.";
        else $this->view->message = $message;

        $this->view->render('error/index', 'error/inc/header', 'error/inc/footer');
    }
}

 ?>