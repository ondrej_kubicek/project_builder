<?php 

class All_Model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    private function countDirecotriesAndFiles($path)
    {
        $root = $path;

        $iter = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($root, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST,
            RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
        );

        $stats = array('project_name' => substr($path, 3),'dirs' => 0, 'files' => 0);
        foreach ($iter as $path => $dir) {
            if ($dir->isDir()) {
                $stats['dirs']++;
            } else {
                $stats['files']++;
            }
        }
        return $stats;
    }

    public function getAllProjects()
    {
        $projects = array();
        $dirs = array_filter(glob('../*'), 'is_dir');
        $dirs = array_diff($dirs, ['../'.ROOT_FOLDER]);

        foreach ($dirs as $key => $dir) {
            $projects[] = $this->countDirecotriesAndFiles($dir);
        }
        return $projects;
    }
}
 ?>