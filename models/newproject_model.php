<?php 

class NewProject_Model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function create()
    {
        $form = new Form();
        $form   ->post('proj_name', 'Project Name')
                ->validate('checkEmpty')
                ->post('directories', 'dirs')
                ->postCheckbox('generate_database', 'Database')
                ->post('default_model_count')
                ->post('custom_model_count');

        for ($i=1; $i <= $form->fetch('default_model_count'); $i++) { 
            $form->postCheckbox('i_default_model_' . $i);
        }

        for ($i=1; $i <= $form->fetch('custom_model_count'); $i++) { 
            $form->postCheckbox('i_custom_model_' . $i);
        }

        $generateDatabase = $form->fetch('generate_database');

        if ($generateDatabase) $generateTables = $form->postCheckbox('create_tables')->fetch('create_tables');
        
        if (!empty($form->submit())) return $form->submit();

        $name = $form->fetch('proj_name');
        $dirs = $form->fetch('directories');

        if (!empty($dirs)){
            $dirs = rtrim($dirs, ';');
            $dirs = explode(';', $dirs);
        }
        else 
            $dirs = array();

        if (!file_exists('../'.$name)) {
            $default_dirs = array('config', 'controllers', 'libs/Form', 'models', 'public', 'utils', 'public/css', 'public/images', 'public/js', 'views');
            $default_files = array('public/css/default.css', 'controllers/error.php');
            $default_views = array('index', 'error');
            $dirs = array_merge($dirs,$default_views);

            foreach ($default_dirs as $dir) {
                mkdir('../'.$name.'/'.$dir, 0777, true);
            }

            $views_path = '../'.$name.'/views/';
            $controllers_path = '../'.$name.'/controllers/';
            $models_path = '../'.$name.'/models/';

            $defaultControllerContent = '<?php

class {name} extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->view->title = \'Controller\';

        $this->view->render(\'{path_to_view}\');
    }
}
 ?>';           $defaultModelContent = '<?php 

class {name} extends Model
{
    function __construct()
    {
        parent::__construct();
    }
}
 ?>';       
            $i = 1;
            $j = 1;
            foreach ($dirs as $dir) { // Creating view files and default controllers for each view
                mkdir($views_path.$dir.'/js', 0777, true);
                $this->justCreateFile($views_path.$dir.'/index.php');

                if (!in_array($dir, $default_views)) {
                    // Create custom model files
                    if ($form->fetch('i_custom_model_' . $j)) {
                        $temp = $dir;
                        $temp[0] = strtoupper($dir[0]);
                        $temp .= '_Model';
                        $currentModelContent = str_replace('{name}', $temp, $defaultModelContent);
                        
                        $this->createFileWithContent($models_path.$dir.'_model.php', $currentModelContent);
                    }
                    $j++;
                    // Create custom view files
                    $temp = $dir;
                    $temp[0] = strtoupper($dir[0]);
                    $currentControllerContent = str_replace('{name}', $temp, $defaultControllerContent);
                    $currentControllerContent = str_replace('{path_to_view}', $dir.'/index', $currentControllerContent);
                    $this->createFileWithContent($controllers_path.$dir.'.php', $currentControllerContent);
                } else {
                    // Create default model files
                    if ($form->fetch('i_default_model_' . ($i))) {
                        $temp = $default_views[$i-1];
                        $temp[0] = strtoupper($default_views[$i-1][0]);
                        $temp .= '_Model';
                        $currentModelContent = str_replace('{name}', $temp, $defaultModelContent);
                        echo htmlspecialchars($currentModelContent).'<br>';
                        echo $models_path.$default_views[$i-1].'_model.php'.'<br>';
                        echo $temp.'<br>';
                        $this->createFileWithContent($models_path.$default_views[$i-1].'_model.php', $currentModelContent);
                    }
                    $i++;
                }
                
            }
            
            $sth = $this->db->prepare("SELECT filename, extension, content, target_dir FROM contents");
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $sth->execute();
            $data=$sth->fetchAll();

            for ($i=0; $i < count($data); $i++) {
                $target_dir = $data[$i]['target_dir'];
                if ($target_dir == 'root') $target_path = '../'.$name.'/';
                else $target_path = '../'.$name.'/'.$target_dir.'/';

                $filename = $target_path.$data[$i]['filename'].$data[$i]['extension'];
                $this->createFileWithContent($filename, $data[$i]['content']);
            }

            if ($generateDatabase) {
                $this->generateDatabase($name);
                if ($generateTables) $this->generateDatabaseTables($name);
            }
            $this->createConfigFile($name, $name);

            return array('' => array('statusCode' => CodeEnum::PROJECT_OK));
        } else {
          return array('' => array('statusCode' => CodeEnum::PROJECT_FAILED));
        }
    }

    private function justCreateFile($path)
    {
        fclose(fopen($path, 'x'));
    }

    private function createFileWithContent($path, $content) {
        $file = fopen($path, 'a');
        fwrite($file, $content);
        fclose($file);
    }

    private function generateDatabase($name, $charset='utf8', $collate='utf8_czech_ci')
    {
        $sth = $this->db->prepare("CREATE DATABASE IF NOT EXISTS $name CHARACTER SET $charset COLLATE $collate");
        $sth->execute();
        $sth->closeCursor();
    }

    private function generateDatabaseTables($dbName)
    {
        $sth = $this->db->prepare("USE $dbName");
        $sth->execute();
        $sth->closeCursor();
        $sth = $this->db->prepare("CREATE TABLE users (id int(11) PRIMARY KEY AUTO_INCREMENT, login varchar(25) NOT NULL UNIQUE, password varchar(64) NOT NULL, salt varchar(64) NOT NULL, role ENUM('moderator', 'admin') NOT NULL DEFAULT 'moderator')");
        $sth->execute();
        $sth->closeCursor();
    }

    private function createConfigFile($name, $dbName='') {
    $path = '../'.$name.'/config/config.php';
    $file = fopen($path, 'a');
    fwrite($file, "<?php
// DATABASE SETTINGS
define ('DB_TYPE', 'mysql');
define ('DB_HOST', 'localhost');
define ('DB_NAME', '" . $dbName . "');
define ('DB_USER', 'root');
define ('DB_PASS', '');

define('DB_SETTINGS', DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8');

// PATHS
define('URL', 'http://localhost/pb/" . $name . "/');
define('LIBS', 'libs/');

//SESSION VARIABLES
define('S_EXPIRE', 0);
?>");
    fclose($file);
    }
}
 ?>