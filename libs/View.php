<?php 

/**
 * Basic View class.
 */
class View
{
    /**
     * Renders the view passed in name parameter. If you want no header or footer, pass null in appropriate parameter.
     * 
     * @param  string  $name        Name of view to be rendered
     * @param  string  $headerFile  Set to render custom header file
     * @param  string  $footerFile  Set to render custom footer file
     */
    public function render($name, $headerFile='header', $footerFile='footer')
    {
        if ($headerFile != null) require 'views/' . $headerFile . '.php';
        require 'views/' . $name . '.php';
        if ($footerFile != null) require 'views/' . $footerFile . '.php';
    }
}

 ?>