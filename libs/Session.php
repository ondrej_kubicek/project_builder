<?php 

/**
 * Wrapper class for $_SESSION.
 */
class Session
{
    public static function init()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    }

    public static function destroy()
    {
        session_unset();
        session_destroy();
        $_SESSION = array();
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function setSystemMessage($key, $value)
    {
        if (is_array($value)) {
            foreach ($value as $name => $code) {
                $_SESSION['messages'][$key][$name] = $code;
            }
        } else {
            $_SESSION['messages'][$key][] = $value;
        }  
    }

    public static function get($key)
    {
        if (isset($_SESSION[$key]))
            return $_SESSION[$key];

        return false;
    }

    public static function getSystemMessage($key, $unset=false) {
        if (isset($_SESSION['messages'][$key]))
            return $_SESSION['messages'][$key];

        return false;
    }

    public static function unsetKey($key) {
        unset($_SESSION[$key]);
    }

    public static function unsetSystemMessage($key) {
        unset($_SESSION['messages'][$key]);
    }

    /**
     * Regenerate current session's id and (Optionally) IP address and User Agent for current user.
     * 
     * @param  boolean $reload  Reloads old IP address and User Agent if set to true
     */
    public static function regenerate($reload=false)
    {
        if(!isset($_SESSION['IPaddress']) || $reload)
            $_SESSION['IPaddress'] = $_SERVER['REMOTE_ADDR'];

        if(!isset($_SESSION['userAgent']) || $reload)
            $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];

        if (S_EXPIRE) 
            $_SESSION['EXPIRES'] = time() + S_EXPIRE;
        
        if(session_status() == PHP_SESSION_ACTIVE)
            session_regenerate_id(true);

        if (session_status() == PHP_SESSION_NONE)
            session_start();
        
    }

    /**
     * Checkse if there are no attempts to attack current session.
     * 
     * @param  boolean $userId  Indicates if the current user_id exists in database
     * @return boolean          If no errors occur return true. If any errors occur, save them to Session variables and returns false
     */
    public static function check($userId)
    {
        if (isset($_SESSION['user_id']) && isset($_SESSION['IPaddress']) && isset($_SESSION['userAgent'])) {
            if(isset($_SESSION['user_id']) && !is_numeric($_SESSION['user_id']))
                $_SESSION['messages'][CodeEnum::SESSION][] = array('statusCode' => CodeEnum::SES_UID);

            if($_SESSION['IPaddress'] != $_SERVER['REMOTE_ADDR'])
                $_SESSION['messages'][CodeEnum::SESSION][] = array('statusCode' => CodeEnum::SES_IP);

            if($_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT'])
                $_SESSION['messages'][CodeEnum::SESSION][] = array('statusCode' => CodeEnum::SES_UA);

            if(!$userId)
                $_SESSION['messages'][CodeEnum::SESSION][] = array('statusCode' => CodeEnum::SES_NOT_LOGGED);

            if (S_EXPIRE) {
                if($_SESSION['EXPIRES'] < time()) {
                    $_SESSION['messages'][CodeEnum::SESSION][] = array('statusCode' => CodeEnum::SES_EXP);
                }
            }
            
            if (!empty($_SESSION['messages'][CodeEnum::SESSION]))
                return false;

            return true;
        }
        return false;
    }
}
 ?>