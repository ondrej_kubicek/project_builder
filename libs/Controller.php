<?php 

/**
 * Basic Controller class that links new View when created.
 */
class Controller
{
    function __construct()
    {
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

        $this->codeEnum = new CodeEnum($lang);
        $this->view = new View();
        $this->view->codeEnum = $this->codeEnum;
    }

    /**
     * Loads the model file for controller.
     * 
     * @param  String $name name of the model
     * @param  String $path location of the models directory
     */
    public function loadModel($name, $modelPath = 'models/')
    {
        $path = $modelPath . $name . '_model.php';

        if(file_exists($path)) {
            require $modelPath . $name . '_model.php';

            $modelName = $name . '_Model';
            $this->model = new $modelName();

        }
    }
}

 ?>