<?php 

/**
 * Class providing static methods to hash data.
 */
class Hash
{
    /**
     * Hash the given data with given salt using the given algorithm.
     * 
     * @param string $data      The data to encode
     * @param string $salt      String used to hash data
     * @param string $algo      The algorithm (md5, sha1, whirpool, etc.)
     * @return string           Final hashed data
     */
    public static function create($data, $salt, $algo='sha256')
    {
        $context = hash_init($algo, HASH_HMAC, $salt);
        hash_update($context, $data);

        return hash_final($context);
    }

    /**
     * Generates salt of given length from given keyspace.
     * 
     * @param  integer $length   Length of final string
     * @param  string  $keyspace Possible characters that can be used to create salt
     * @return string            Final salt string
     */
    public static function generateSalt($length=64, $keyspace='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $charactersLength = strlen($keyspace);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $keyspace[rand(0, $charactersLength - 1)];
        }
        
        return $randomString;
    }
}

 ?>