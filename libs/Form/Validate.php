<?php 

/**
 * Class provides various methods to validate the input data.
 * Every methods returns the result in following format:
 *     array('statusCode' => CodeEnum::VAL_METHOD_NAME) - if method doesn't accepts any arguments
 *     array('statusCode' => CodeEnum::VAL_METHOD_NAME, 'arg => $arg') - if method accepts any arguments
 */
class Validate
{
    /**
     * Checks if string is longer then required number of characters.
     * 
     * @param  String   $data           Value to check its length
     * @param  int      $arg            Minimal number of characters
     * @return mixed                    Array or nothing
     */
    public function minLength($data, $arg)
    {
        if(strlen($data) < $arg) 
            return array('statusCode' => CodeEnum::VAL_MINLEN, 'arg' => $arg);
    }

    /**
     * Checks if string is longer then required number of characters.
     * 
     * @param  String   $data           Value to check its length
     * @param  int      $arg            Maximal number of characters
     * @return mixed                    Array or nothing
     */
    public function maxLength($data, $arg)
    {
        if(strlen($data) > $arg) 
            return array('statusCode' => CodeEnum::VAL_MAXLEN, 'arg' => $arg);
    }

    /**
     * Checks if value is integer.
     * 
     * @param  string   $data           Value to check if it is an integer
     * @return mixed                    Array or nothing
     */
    public function digit($data)
    {
        if(filter_var($data, FILTER_VALIDATE_INT) === false) 
            return array('statusCode' => CodeEnum::VAL_DIGIT);
    }

    /**
     * Checks if given value is valid e-mail address.
     * 
     * @param  string $data             Value to check if it is a valid e-mail address
     * @return mixed                    Array or nothing
     */
    public function email($data)
    {
        if (filter_var($data, FILTER_VALIDATE_EMAIL) === false)
            return array('statusCode' => CodeEnum::VAL_EMAIL);
    }

    /**
     * Strictly checks if given value is contained in allowed values specified by array.
     * 
     * @param  string $data             Value to check if it is an allowed value
     * @param  array  $values           Array of allowed values
     * @return mixed                    Array or nothing
     */
    public function values($data, $values)
    {
        if (!in_array($data, $values, true)) 
            return array('statusCode' => CodeEnum::VAL_VALUES);
    }

    /**
     * Checks if value is empty.
     * 
     * @param  string $data             Value to check if it is empty
     * @return mixed                    Array or nothing
     */
    public function checkEmpty($data)
    {
        if (empty($data)) 
            return array('statusCode' => CodeEnum::VAL_EMPTY);
    }

    /**
     * Throws exception when called method that doesn't exist.
     * 
     * @param  string $name     Name of method that has been called
     * @param  array $args      Optional arguments
     * @return                  Error message if called method doesn't exist
     */
    public function __call($name, $args)
    {
        return array('statusCode' => CodeEnum::VAL_CALL_ERROR, 'arg' => __CLASS__);
        
    }
}

 ?>