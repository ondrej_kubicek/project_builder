<?php 

/**
 * Wrapper class for PDO database access.
 */
class Database extends PDO
{
    function __construct($DB_SETTINGS, $DB_USER, $DB_PASS)
    {
        parent::__construct($DB_SETTINGS, $DB_USER, $DB_PASS);
    }

    public function selectOne($sql, $array=array(), $fetchMode=PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        $sth->setFetchMode($fetchMode);

        foreach ($array as $key => $value) {
            $sth->bindValue($key, $value);
        }

        $sth->execute();
        return $sth->fetch();
    }

    public function select($sql, $array=array(), $fetchMode=PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        $sth->setFetchMode($fetchMode);

        foreach ($array as $key => $value) {
            $sth->bindValue($key, $value);
        }

        $sth->execute();
        return $sth->fetchAll();
    }

    public function insert($table, $data)
    {
        ksort($data);

        $fieldNames = implode(',', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));;
        $sth = $this->prepare("INSERT INTO $table($fieldNames) VALUES($fieldValues)");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        
        $sth->execute();
    }

    public function update($table, $data, $where)
    {
        ksort($data);

        $fieldDetails = null;

        foreach ($data as $key => $value) {
            $fieldDetails .= "$key = :$key,";
        }

        $fieldDetails = rtrim($fieldDetails, ',');

        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
        
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        $sth->execute();
    }

    /*
    public function update($table, $data, $where)
    {
        ksort($data);

        $fieldDetails = null;
        $whereArray = null;

        foreach ($data as $key => $value) {
            $fieldDetails .= "$key = :$key,";
        }

        foreach ($where as $key => $value) {
            $whereArray .= "$key = :$key";
        }

        $fieldDetails = rtrim($fieldDetails, ',');

        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $whereArray");
        
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        foreach ($where as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        $sth->execute();
        $sth->closeCursor();
    }
     */
    public function delete($table, $where, $limit=1)
    {
        return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
    }
    
    /**
     * Get salt for user from database.
     * 
     * @param  string $username     User's username to look salt for
     * @return mixed                User's salt if username exists, otherwise returns false
     */
    public function getSalt($username)
    {
        $sth = $this->prepare("SELECT salt FROM users WHERE username = :username");
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array(":username" => $username));

        $count = $sth->rowCount();

        if ($count == 1)
        {
            return $sth->fetch();
        }
        else
        {
            return false;
        }
    }

    /**
     * Check if password is valid
     * 
     * @param  string $username     User's username to check if it is a valid user
     * @param  array  $password     User's hashed password via Hash::create() method to check if it is a valid user
     * @return mixed                Array with user data if credentials entered are valid otherwise false
     */
    public function checkUser($username, $password)
    {
        $sth = $this->prepare("SELECT * FROM users WHERE username = :username AND password = :password");
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array(
            ':username' => $username,
            ':password' => $password
        ));

        $data = $sth->fetch();
        $count = $sth->rowCount();
        if ($count == 1)
        {
            return $data;
        }
        else
        {
            return false;
        }
    }
}
 ?>