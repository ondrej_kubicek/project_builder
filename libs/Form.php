<?php

/**
 * Class for handling and validating form requests.
 */
class Form
{
    /** @var array $_postData Stores the posted data */
    private $_postData = array();
    
    /** @var array $_currentItem The immediately posted item */
    private $_currentItem = null;

    /** @var array $_previousItem The previously posted item */
    private $_previousItem = null;

    /** @var string $_currentItemName The name of immediately posted item*/
    private $_currentItemName = '';

    /** @var string $_previousItemName The name of previously posted item*/
    private $_previousItemName = '';

    /** @var object $_validator The validator object */
    private $_validator = array();

    /** @var array $_error Holds the current forms errors */
    private $_error = array();

    /**
     * The constructor. Sets up the instance of Validate class.
     */
    function __construct()
    {
        $this->_validator = new Validate();
    }

    /**
     * Fills Form instance with data from POST request defined by field.
     * 
     * @param  string $field        Name of POST value to fetch
     * @param  string $fieldName    Name of current field to be displayed if any errors occurs. Can leave this empty, if you don't want to validate data for these data
     * @param  int $filter          The ID of filter to be applied. See the list of filters at http://php.net/manual/en/filter.filters.php
     *                              If omitted, FILTER_SANITIZE_SPECIAL_CHARS is used by default. Another usefull filter can be FILTER_SANITIZE_ENCODED for getting URL strings.
     * @return object               Form object
     */
    public function post($field, $fieldName='', $filter=FILTER_SANITIZE_SPECIAL_CHARS)
    {
        $this->_postData[$field] = filter_input(INPUT_POST, $field, $filter);

        $this->_previousItem = $this->_currentItem;
        $this->_previousItemName = $this->_currentItemName;
        $this->_currentItem = $field;
        $this->_currentItemName = $fieldName;
        
        return $this;
    }

    /**
     * Fills Form instance with data from POST request defined by field. This method is used to fetch POST data from checkboxes.
     * 
     * @param  string $field        Name of POST value to fetch
     * @param  string $fieldName    Name of current field to be displayed if any errors occurs. Can leave this empty, if you don't want to validate data for these data
     * @return object               Form object
     */
    public function postCheckbox($field, $fieldName='')
    {
        $this->_postData[$field] = isset($_POST[$field]);
        $this->_currentItemName = $fieldName;
        $this->_currentItem = $field;

        return $this;
    }
    /**
     * Return the posted data. If fieldName is specified by string the only one value is returned else the whole array is returned.
     * 
     * @param  mixed $fieldName     (Optional) Name of POST field to fetch
     * @return mixed                String or array
     */
    public function fetch($fieldName = false)
    {
        if($fieldName) {
            if(isset($this->_postData[$fieldName])) return $this->_postData[$fieldName];
            else return false;
        } else {
            return $this->_postData;
        }
    }

    public function equals() {

        if ($this->_postData[$this->_currentItem] !== $this->_postData[$this->_previousItem])
            $this->_error[$this->_previousItemName] = array('statusCode' => CodeEnum::VAL_EQUALS);
        return $this;
    }

    /**
     * Validate data sent throught POST request.
     * 
     * @param  string   $typeOfValidator    Method from Form/Validate class
     * @param  mixed    $arg                Value to validate against
     * @return object                       Form object
     */
    public function validate($typeOfValidator, $arg = null)
    {
        if($arg == null) {
            $error = $this->_validator->{$typeOfValidator}($this->_postData[$this->_currentItem]);
        } else {
            $error = $this->_validator->{$typeOfValidator}($this->_postData[$this->_currentItem], $arg);
        }

        if($error) 
            $this->_error[$this->_currentItemName] = $error;

        return $this;
    }

    /**
     * Handles the form and throws an exception upon an error.
     * 
     * @return string       Empty string if no errors occured
     */
    public function submit()
    {
        return $this->_error;
    }
}

 ?>