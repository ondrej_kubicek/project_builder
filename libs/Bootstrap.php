<?php

class Bootstrap {
    
    private $_url = null;
    private $_controller = null;

    private $_controllerPath = 'controllers/';
    private $_modelPath = 'models/';
    private $_defaultFile = 'index.php';
    private $_errorFile = 'error.php';

    function __construct() {}

    public function init()
    {
        $this->_getUrl();

        // load the default controller if no URL is set
        if (empty($this->_url[0])) {
            $this->_loadDefaultController();
            return false;
        }

        $this->_loadExistingController();
        $this->_callControllerMethod();
    }

    /**
     * _getUrl - Fetches the $_GET from URL.
     */
    private function _getUrl()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
    }

    /**
     * Loads if there is no $_GET parameter passed.
     */
    private function _loadDefaultController()
    {
        require $this->_controllerPath . $this->_defaultFile;
        $this->_controller = new Index();
        $this->_controller->index();
    }

    /**
     * Loads an existing controller if there IS a $_GET parameter passed.
     * 
     * @return boolean     
     */
    private function _loadExistingController()
    {
        $file = $this->_controllerPath . $this->_url[0] . '.php';

        if (file_exists($file)) {
            require $file;
            $this->_controller = new $this->_url[0];
            $this->_controller->loadModel($this->_url[0], $this->_modelPath);
        } else {
            $this->_error();
        }
    }

    /**
     * Calls a controllers method if is passed via $_GET in URL.
     */
    private function _callControllerMethod()
    {
        $length = count($this->_url);

        if ($length > 1) {
            if (!method_exists($this->_controller, $this->_url[1])) {
                $this->_error();

                return;
            }
        }

        switch ($length) {
            case 5: 
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);
                break;
            case 4:
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);
                break;
            case 3:
                $this->_controller->{$this->_url[1]}($this->_url[2]);
                break;
            case 2:
                $this->_controller->{$this->_url[1]}();
                break;
            case 1:
                $this->_controller->index();
                break;
            default:
                $this->_error();
                break;
        }
    }

    /**
     * Displays an error page if there is any controller or method passed via URL that doesn't exist.
     * 
     * @return boolean
     */
    private function _error() {
        require $this->_controllerPath . $this->_errorFile;
        $this->_controller = new Error();
        $this->_controller->index();

        exit;
    }

    /**
     * (Optional) Set a custom path to controllers.
     * 
     * @param String $path custom path for controllers folder
     */
    public function setControllerPath($path)
    {
        $this->_controllerPath = trim($path, '/') . '/';
    }

    /**
     * (Optional) Set a custom path to models.
     * 
     * @param String $path custom path for models folder
     */
    public function setModelPath($path)
    {
        $this->_modelPath = trim($path, '/') . '/';
    }

    /**
     * (Optional) Set a custom path to error file.
     * 
     * @param String $path use the file name of your controller, eg. index.php
     */
    public function setDefaultFile($path)
    {
        $this->_defaultFile = trim($path, '/');
    }

    /**
     * (Optional) Set a custom path to error file.
     * 
     * @param String $path use the file name of your controller, eg. error.php
     */
    public function setErrorFile($path)
    {
        $this->_errorFile = trim($path, '/');
    }
}
 ?>