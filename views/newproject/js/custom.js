$(function() {
    $('#i_generate_database').click(function() {
        if (this.checked) {
            $('<div>').attr({
                id:     'dbGenAdditional'
            }).appendTo('.database');

            var inp = $('<input />').attr({
                type:   'checkbox',
                name:   'create_tables',
                id:     'i_create_tables',
                style:  'margin-left: 15px;'
            }).appendTo('#dbGenAdditional');

            var lbl = $('<label />').attr({
                for: 'i_create_tables'
            }).text('Create users table');

            $(inp).after(lbl);
            $(lbl).after('<br>');
        } else {
            $('#dbGenAdditional').remove();
        }
    });
    var models = $('#custom_models');
    var count;
    var hit_count = 0;
    var last_count = 0;

    $('#i_dirs').keyup(function(e) {
        var data = $('#i_dirs').val();
        content = data.split(';');
        count = content.length;

        if (last_count == 0)
            last_count = count;

        if (e.which == 192 && hit_count == 1) {
            $('#i_dirs').val(data.slice(0,-1));
            return;
        }

        if (e.which == 8 || e.which == 46) { //backspace or delete
            if (content[0] == '') {
                $('#custom_models').empty();
                $('#i_custom_model_count').val(0);
            }

            if (content.length < last_count) {
                do {
                    $('#i_custom_model_' + (last_count)).remove();
                    $('label[for="i_custom_model_' + (last_count) + '"] + br').remove();
                    $('label[for="i_custom_model_' + (last_count) + '"]').remove();
                    last_count--;
                
                    $('#i_custom_model_count').val(models.children().length / 3); // #custom_models child_count / 3
                } while(content.length != last_count)
            }
        }
        
        if (e.which == 192) {// ;
            hit_count++;
            last_count = count;
        } else {
            hit_count = 0;
        }

        for (var i = 0; i < count; i++) {
            if (content[i] != '') {
                
                if ($('#i_custom_model_' + (i+1)).length) {
                    
                    $('label[for="i_custom_model_' + (i+1) + '"]').text(content[i]);
                } else {
                    var cb = $('<input />').attr({
                        type:   'checkbox',
                        name:   'i_custom_model_' + (i+1),
                        id:     'i_custom_model_' + (i+1),
                        style:  'margin-left: 15px;'
                    }).appendTo(models);

                    $('#i_custom_model_count').val(count);

                    var lbl_cb = $('<label />').attr({
                        for: 'i_custom_model_' + (i+1)
                    }).text(content[i]);

                    $(cb).after(lbl_cb);
                    $(lbl_cb).after('<br>');
                }
            }
        }
    });
});

