<?php 
echo $this->message;
Session::init();

 ?>
<div style="clear:both;">
    <form action="newproject/create" method="post">
    <div class="left">
    	<label><input type="text" name="proj_name" value="" placeholder="PROJECT NAME" /></label> <br>
    	<label><input type="text" name="directories" value="" placeholder="View dirs divided by ;" id="i_dirs" /></label><br>
    	<input type="submit" value="Create" style="clear: both;"/>
    </div>
    <div class="right">
        <h2>Database</h2>
        <div class="database">
            <input type="checkbox" name="generate_database" class="dbGenerate" id="i_generate_database"><label for="i_generate_database">Generate database</label><br />
        </div>
        <h2>Models</h2>
        <div class="models">
            <div id="default_models">
                <input type="checkbox" name="i_default_model_1" id="i_default_model_1"><label for="i_default_model_1">Index model</label><br />
                <input type="checkbox" name="i_default_model_2" id="i_default_model_2"><label for="i_default_model_2">Error model</label><br />
            </div>
            <input type="hidden" name="default_model_count" value="2">
            <div id="custom_models"></div>
            <input type="hidden" name="custom_model_count" id="i_custom_model_count" value="0">
        </div>
    </div>
    </form>
    
</div>
<div style="clear:both; color: #f00;">
<?php
    if(!empty(Session::getSystemMessage(CodeEnum::FORM))) {
        foreach (Session::getSystemMessage(CodeEnum::FORM) as $key => $value) {
            echo $key ;
            echo $this->codeEnum->getMessage($value['statusCode']). (isset($value['arg']) ? $value['arg'] : '') . '<br>';
        }
        Session::unsetSystemMessage(CodeEnum::FORM);
    }
?>
    
</div>
<?php 
/*Session::init();
Session::regenerate();
echo session_id() . '<br>';

//Session::unsetSystemMessage('userAgent');
//unset($_SESSION['messages']['session']['userAgent']);
//print_r($_SESSION);
//unset($_SESSION['userAgent']);
Session::set('user_id', 1);
Session::set('userAgent', 'xy');
if (!Session::check(1))
    echo 'ě';
Tools::dFormatedPrint($_SESSION);
if(!empty(Session::getSystemMessage(CodeEnum::SESSION))) {
    foreach (Session::getSystemMessage(CodeEnum::SESSION) as $key => $value) {
        echo '<br>';
        echo $this->codeEnum->getMessage($value['statusCode']). (isset($value['arg']) ? $value['arg'] : '') . '<br>';
    }
    Session::unsetSystemMessage(CodeEnum::SESSION);
}*/
//Session::destroy();
 ?>