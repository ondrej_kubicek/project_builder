<!DOCTYPE html>
<html>
<head>
	<title><?php echo $this->title; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/css/default.css">
	<script src="<?php echo URL; ?>public/js/jquery.js" type="text/javascript"></script>
	<?php
		if (isset($this->js)) {
			foreach ($this->js as $js) {
				echo '<script src="'. URL.'views/'.$js.'" type="text/javascript"></script>';
			}
		}
	?>
</head>
<body>
<?php Session::init(); ?>
	<div class="wrap">
		<div id="header">
			<a href="<?php echo URL; ?>index"><h1><?php echo PROJECT_NAME;?></h1></a>
		</div>
	</div>
	<div class="wrap">
		<div id="menu">
			<menu>
				<li><a href="all">View all projects</a></li>
				<li><a href="newproject">Create new project structure</a></li>
				<li><a href="edit">Edit project</a></li>
			</menu>
		</div>
	</div>
	<div id="content">
		
